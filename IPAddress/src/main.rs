enum IPAddrKind {
    V4(u8, u8, u8, u8),
    V6(u8, u8, u8, u8),
}
fn main() {
    let four: IPAddrKind = IPAddrKind::V4;
    let six: IPAddrKind = IPAddrKind::V6;
    let localhost: IPAddrKind = IPAddrKind::V4 {
        kind: IPAddrKind::V4,
        address: String::from("127.0.0.1")
    };
}
struct IPAddr {
    kind: IPAddrKind,
    address: String,
}

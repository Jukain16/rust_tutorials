use std::io;
fn is_consonnant(c: char) -> bool {
    ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'].contains(&c)
}
fn main() {
    println!("Enter a string.");
    let mut word = String::new();
    io::stdin().read_line(&mut word)
        .expect("Failed to read line."); 
    word.remove(word.len() - 1);
    let pig = word.chars().next()
        .expect("Empty string.");
    if is_consonnant(pig) {
        word += "-hay";
    }
    else {
        word.remove(0);
        word += &format!("-{}ay", pig);
    }
    println!("{}", word);
}

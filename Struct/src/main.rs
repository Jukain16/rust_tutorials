struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}
fn main() {
    let mut user1: User = User {
        email: String::from("bogdan@mail.com"),
        username: String::from("bogdan123"), // can be in any order
        active: true,
        sign_in_count:1
    };
    let name: String = user1.username;
    user1.username = String::from("wallace123");
    let user2: User = build_user(
        String::from("kyle@gmail.com"), 
        String::from("kyle426")
        );
    let user3: User = User {
        email: String::from("lauren@yahoo.com"),
        username: String::from("lauren23"),
        ..user2
    };
}
fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

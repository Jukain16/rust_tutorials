use std::io;
use std::collections::HashMap;
fn main() {
    println!("Enter name.");
   let mut map: HashMap<String, Vec<String>> = HashMap::new();
   loop {
       println!("a) add a dept");
       println!("b) add employee to an existing dept");
       println!("c) view all people in a dept");
       println!("d) view all people in the company by dept");
       let mut input = String::new();
       io::stdin().read_line(&mut input)
           .expect("Failed to read line.");
       let input: char = match input.trim().parse() {
           Ok(c) => c,
           Err(_) => continue,
       };
       match input {
           'a' => add_dept(&mut map), 
           'b' => add_employee_to_dept(&mut map),
           'c' => print_employees_for_dept(&map), 
           'd' => println!("The company directory: {:?}", map),
           _ => panic!("Invalid input, goodbye!"),
       }
   }
}
fn add_dept(map: &mut HashMap<String, Vec<String>>) {
    println!("enter the name of dept");
    let mut dept = String::new();
    io::stdin().read_line(&mut dept)
        .expect("Failed to read line.");
    let dept: String = match dept.trim().parse() {
        Ok(s) => s,
        Err(_) => panic!("Invalid dept input!"),
    };
    map.entry(dept).or_insert(Vec::new());
    println!("company depts {:?}", map);
    println!();
}
fn add_employee_to_dept(map: &mut HashMap<String, Vec<String>>) {
    println!("enter the name of dept");
    let mut dept = String::new();
    io::stdin().read_line(&mut dept)
        .expect("Failed to read line");
    let dept: String = match dept.trim().parse() {
        Ok(s) => s,
        Err(_) => panic!("Invalid dept input."),
    };
    let employees: Option<&mut Vec<String>> = map.get_mut(&dept);
    let mut employee = String::new();
    io::stdin().read_line(&mut employee)
        .expect("Failed to read line.");
    let employee: String = match employee.trim().parse() {
        Ok(s) => s, 
        Err(_) => return,
    };
    if let Some(employees_vector) = employees {
        employees_vector.push(employee.to_string());
    } else {
        panic!("invalid input");
    }
    println!("company depts: {:?}", map);
    println!();
}
fn print_employees_for_dept(map: &HashMap<String, Vec<String>>) {
    println!("enter the name of the dept");
    let mut dept = String::new();
    io::stdin().read_line(&mut dept)
        .expect("Failed to read line.");
    let dept: String = match dept.trim().parse() {
        Ok(s) => s,
        Err(_) => panic!("Invalid dept input"),
    };
    let employees = map.get(&dept);
    println!("employees for dept: {}, are: {:?}", dept, employees);
}

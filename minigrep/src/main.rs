use std::env; // collects input
use std::fs;
fn main() {
    let args: Vec<String> = env::args().collect(); //passes input into Vector
    let config = parse_config(&args);
    println!("{:?}", args);
    let query = &args[1];
    let filename = &args[2];
    println!("Searching for {}", config.query);
    println!("In file {}", config.filename);
    let contents = fs::read_to_string(config.filename)
        .expect("Something went wrong reading the file");
    println!("With text:\n{}", contents);
}
fn parse_config(args: &[String]) -> (&str, &str) {
    let query = &args[1];
    let filename = &args[2];
    (query, filename)
}
struct Config {
    query: String,
    filename: String,

}
